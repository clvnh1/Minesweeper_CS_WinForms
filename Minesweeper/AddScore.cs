﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Minesweeper
{
    public partial class AddScore : Form
    {
        int lastTime, lastWidth, lastHeight, lastMines;

        public AddScore(int time, int width, int height, int mines)
        {
            InitializeComponent();
            lastTime = time; lastWidth = width; lastHeight = height; lastMines = mines;
            
            timeLbl.Text = "T:" + lastTime + "s";
            stuffLbl.Text = "W:" + lastWidth + " H:" + lastHeight + " M:" + lastMines;
        }

        private void submitBtn_Click(object sender, EventArgs e)
        {
            MSweeper.scoreList.Add(new Score(nameBox.Text, lastTime, lastWidth, lastHeight, lastMines));
            Environment.Exit(0);
        }
    }
}

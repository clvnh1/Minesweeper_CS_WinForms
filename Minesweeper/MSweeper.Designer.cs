﻿namespace Minesweeper
{
	partial class MSweeper
	{
		/// <summary>
		/// Erforderliche Designervariable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Verwendete Ressourcen bereinigen.
		/// </summary>
		/// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Vom Windows Form-Designer generierter Code

		/// <summary>
		/// Erforderliche Methode für die Designerunterstützung.
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.menuStrip = new System.Windows.Forms.MenuStrip();
			this.gameToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.newGameToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.resetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.statisticsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
			this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.gameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.newGameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.restartToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.scoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.optionsMenu = new System.Windows.Forms.ToolStripMenuItem();
			this.exitBtn = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.restartBtn = new System.Windows.Forms.Button();
			this.timeText = new System.Windows.Forms.Label();
			this.bombsText = new System.Windows.Forms.Label();
			this.timePassed = new System.Windows.Forms.Timer(this.components);
			this.bottomPanel = new System.Windows.Forms.Panel();
			this.menuStrip.SuspendLayout();
			this.bottomPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip
			// 
			this.menuStrip.BackColor = System.Drawing.SystemColors.Control;
			this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.gameToolStripMenuItem1});
			this.menuStrip.Location = new System.Drawing.Point(0, 0);
			this.menuStrip.Name = "menuStrip";
			this.menuStrip.Size = new System.Drawing.Size(210, 24);
			this.menuStrip.TabIndex = 0;
			// 
			// gameToolStripMenuItem1
			// 
			this.gameToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.newGameToolStripMenuItem1,
			this.resetToolStripMenuItem,
			this.toolStripSeparator3,
			this.statisticsToolStripMenuItem,
			this.toolStripSeparator4,
			this.optionsToolStripMenuItem,
			this.exitToolStripMenuItem});
			this.gameToolStripMenuItem1.Name = "gameToolStripMenuItem1";
			this.gameToolStripMenuItem1.Size = new System.Drawing.Size(50, 20);
			this.gameToolStripMenuItem1.Text = "Game";
			// 
			// newGameToolStripMenuItem1
			// 
			this.newGameToolStripMenuItem1.Name = "newGameToolStripMenuItem1";
			this.newGameToolStripMenuItem1.ShortcutKeys = System.Windows.Forms.Keys.F2;
			this.newGameToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
			this.newGameToolStripMenuItem1.Text = "&New game";
			this.newGameToolStripMenuItem1.Click += new System.EventHandler(this.restartBtn_Click);
			// 
			// resetToolStripMenuItem
			// 
			this.resetToolStripMenuItem.Name = "resetToolStripMenuItem";
			this.resetToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F3;
			this.resetToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.resetToolStripMenuItem.Text = "&Reset";
			this.resetToolStripMenuItem.Click += new System.EventHandler(this.resetToolStripMenuItem_Click);
			// 
			// toolStripSeparator3
			// 
			this.toolStripSeparator3.Name = "toolStripSeparator3";
			this.toolStripSeparator3.Size = new System.Drawing.Size(149, 6);
			// 
			// statisticsToolStripMenuItem
			// 
			this.statisticsToolStripMenuItem.Enabled = false;
			this.statisticsToolStripMenuItem.Name = "statisticsToolStripMenuItem";
			this.statisticsToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F4;
			this.statisticsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.statisticsToolStripMenuItem.Text = "&Statistics";
			// 
			// toolStripSeparator4
			// 
			this.toolStripSeparator4.Name = "toolStripSeparator4";
			this.toolStripSeparator4.Size = new System.Drawing.Size(149, 6);
			// 
			// optionsToolStripMenuItem
			// 
			this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
			this.optionsToolStripMenuItem.ShortcutKeyDisplayString = "";
			this.optionsToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
			this.optionsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.optionsToolStripMenuItem.Text = "&Options";
			this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsMenu_Click);
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.exitToolStripMenuItem.Text = "E&xit";
			this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitBtn_Click);
			// 
			// gameToolStripMenuItem
			// 
			this.gameToolStripMenuItem.Name = "gameToolStripMenuItem";
			this.gameToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
			this.gameToolStripMenuItem.Text = "&Game";
			// 
			// newGameToolStripMenuItem
			// 
			this.newGameToolStripMenuItem.Name = "newGameToolStripMenuItem";
			this.newGameToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2;
			this.newGameToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.newGameToolStripMenuItem.Text = "&New game";
			this.newGameToolStripMenuItem.Click += new System.EventHandler(this.restartBtn_Click);
			// 
			// restartToolStripMenuItem
			// 
			this.restartToolStripMenuItem.Enabled = false;
			this.restartToolStripMenuItem.Name = "restartToolStripMenuItem";
			this.restartToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F3;
			this.restartToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.restartToolStripMenuItem.Text = "Reset";
			this.restartToolStripMenuItem.Click += new System.EventHandler(this.resetToolStripMenuItem_Click);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(149, 6);
			// 
			// scoresToolStripMenuItem
			// 
			this.scoresToolStripMenuItem.Enabled = false;
			this.scoresToolStripMenuItem.Name = "scoresToolStripMenuItem";
			this.scoresToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F4;
			this.scoresToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.scoresToolStripMenuItem.Text = "&Scores";
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(149, 6);
			// 
			// optionsMenu
			// 
			this.optionsMenu.Name = "optionsMenu";
			this.optionsMenu.ShortcutKeys = System.Windows.Forms.Keys.F5;
			this.optionsMenu.Size = new System.Drawing.Size(152, 22);
			this.optionsMenu.Text = "&Options";
			this.optionsMenu.Click += new System.EventHandler(this.optionsMenu_Click);
			// 
			// exitBtn
			// 
			this.exitBtn.Name = "exitBtn";
			this.exitBtn.Size = new System.Drawing.Size(152, 22);
			this.exitBtn.Text = "E&xit";
			this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
			// 
			// toolStripMenuItem1
			// 
			this.toolStripMenuItem1.Enabled = false;
			this.toolStripMenuItem1.Name = "toolStripMenuItem1";
			this.toolStripMenuItem1.Size = new System.Drawing.Size(44, 20);
			this.toolStripMenuItem1.Text = "&Help";
			// 
			// restartBtn
			// 
			this.restartBtn.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.restartBtn.Location = new System.Drawing.Point(73, 0);
			this.restartBtn.Name = "restartBtn";
			this.restartBtn.Size = new System.Drawing.Size(64, 26);
			this.restartBtn.TabIndex = 1;
			this.restartBtn.Text = "Restart";
			this.restartBtn.UseVisualStyleBackColor = true;
			this.restartBtn.Click += new System.EventHandler(this.restartBtn_Click);
			// 
			// timeText
			// 
			this.timeText.AutoSize = true;
			this.timeText.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.timeText.Location = new System.Drawing.Point(19, 0);
			this.timeText.Name = "timeText";
			this.timeText.Size = new System.Drawing.Size(48, 25);
			this.timeText.TabIndex = 4;
			this.timeText.Text = "000";
			this.timeText.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// bombsText
			// 
			this.bombsText.AutoSize = true;
			this.bombsText.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.bombsText.Location = new System.Drawing.Point(143, 0);
			this.bombsText.Name = "bombsText";
			this.bombsText.Size = new System.Drawing.Size(48, 25);
			this.bombsText.TabIndex = 5;
			this.bombsText.Text = "000";
			this.bombsText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// timePassed
			// 
			this.timePassed.Enabled = true;
			this.timePassed.Interval = 1000;
			this.timePassed.Tick += new System.EventHandler(this.timePassed_Tick);
			// 
			// bottomPanel
			// 
			this.bottomPanel.Controls.Add(this.timeText);
			this.bottomPanel.Controls.Add(this.bombsText);
			this.bottomPanel.Controls.Add(this.restartBtn);
			this.bottomPanel.Location = new System.Drawing.Point(0, 231);
			this.bottomPanel.Name = "bottomPanel";
			this.bottomPanel.Size = new System.Drawing.Size(210, 43);
			this.bottomPanel.TabIndex = 6;
			// 
			// MSweeper
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(210, 274);
			this.Controls.Add(this.bottomPanel);
			this.Controls.Add(this.menuStrip);
			this.DoubleBuffered = true;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MainMenuStrip = this.menuStrip;
			this.MaximizeBox = false;
			this.Name = "MSweeper";
			this.Text = "Minesweeper";
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.MSweeper_Paint);
			this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MSweeper_MouseDown);
			this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MSweeper_MouseMove);
			this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MSweeper_MouseUp);
			this.menuStrip.ResumeLayout(false);
			this.menuStrip.PerformLayout();
			this.bottomPanel.ResumeLayout(false);
			this.bottomPanel.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.MenuStrip menuStrip;
		private System.Windows.Forms.ToolStripMenuItem gameToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem optionsMenu;
		private System.Windows.Forms.ToolStripMenuItem exitBtn;
		private System.Windows.Forms.Button restartBtn;
		private System.Windows.Forms.Label timeText;
		private System.Windows.Forms.Label bombsText;
		private System.Windows.Forms.Timer timePassed;
		private System.Windows.Forms.Panel bottomPanel;
		private System.Windows.Forms.ToolStripMenuItem newGameToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem restartToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripMenuItem scoresToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem gameToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem newGameToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem resetToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
		private System.Windows.Forms.ToolStripMenuItem statisticsToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
		private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
	}
}

